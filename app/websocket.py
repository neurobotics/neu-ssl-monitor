# coding: utf-8
import json
import datetime
import time
import pickle
import memcache
mc = memcache.Client(['127.0.0.1:11211'], debug=0)

ws_list = []

def get_latest_state():
	robots_blue = pickle.loads(mc.get("robots_blue"))
	robots_yellow = pickle.loads(mc.get("robots_yellow"))
	balls = pickle.loads(mc.get("balls"))
	b = {}
	for k,v in robots_blue.iteritems():
		x, y = int(v["x"]) + 3000, int(v["y"]) * -1 + 2000
		x = 800 * x / 6000
		y = 600 * y / 4000
		y = abs(y - 600) # because plane in canvas is y inverted
		a = {k:(x, 600 - y) }
        
        
		b.update(a)

	yl = {}
	for k,v in robots_yellow.iteritems():
		x, y = int(v["x"]) + 3000, int(v["y"]) * -1 + 2000
		x = 800 * x / 6000
		y = 600 * y / 4000
		y = abs(y - 600) # because plane in canvas is y inverted
		a = {k:(x, 600 - y) }
        
		yl.update(a)

	
	bls = {}
	for k,v in balls.iteritems():
		x, y = int(v["x"]) + 3000, int(v["y"]) * -1 + 2000
        x = 800 * x / 6000
        y = 600 * y / 4000
        y = abs(y - 600) # because plane in canvas is y inverted
        a = {k:(x, 600 - y) }

        bls.update(a)
	return {"robots_blue":b, "robots_yellow": yl, "balls":bls}



def handle_websocket(ws):
    ws_list.append(ws)
    
    while True:
        #print "SENDING"
        ws.send(json.dumps(get_latest_state()))
        message = ws.receive()
        if message is None:
            break
