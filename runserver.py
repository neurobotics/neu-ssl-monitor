#!/usr/bin/env python
# coding: utf-8
from gevent.pywsgi import WSGIServer
from geventwebsocket.handler import WebSocketHandler
import sys




from app import my_app

if __name__ == '__main__':
	print "NEU-SSL-MONITOR"
	#host = "172.16.7.141"
	host = "0.0.0.0"
	port = 8000
	try:
		host = sys.argv[1]
		port = int(sys.argv[2])
	except:
		pass
	print "Listening on %s:%s" % (host,port)
	http_server = WSGIServer((host,port), my_app, handler_class=WebSocketHandler)
	http_server.serve_forever()
