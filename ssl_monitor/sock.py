import sys
import socket
import struct
import threading
import pickle


from time import sleep

from proto import messages_robocup_ssl_wrapper_pb2 as pb_wrapper
from proto import messages_robocup_ssl_detection_pb2 as pb_detection
from proto import messages_robocup_ssl_geometry_pb2 as pb_geometry
from proto import messages_robocup_ssl_refbox_log_pb2 as pb_refbox

import env

import config

VISION_IP = config.SSL_VISION_MULTICAST_ADDRESS
VISION_PORT = config.SSL_VISION_MULTICAST_PORT


world = env.World()

class Server(object):
    def __init__(self, serv_ip=VISION_IP,  serv_port=VISION_PORT):
        self.serv_port = serv_port
        self.serv_ip = serv_ip
        self.World = env.World()
    
    def start(self):
        self.__bind()
        self.receiver()

    def __bind(self):

        try:
            print "DEBUG: CREATING SOCKET"
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            mreq = struct.pack("4sl", socket.inet_aton(VISION_IP), socket.INADDR_ANY)
            self.socket.bind(('', VISION_PORT))
            self.socket.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

        except socket.error, err:
            print "ERROR: EXCEPTION DURING INITIALIZING SERVER'S SOCKET - %s" % err
            sys.exit(1)



    def receiver(self):
        print "DEBUG: WAITING FOR DATA"
        while True:
            try:
                msg, who = self.socket.recvfrom(65536)
            except socket.timeout:
                continue
            except socket.error, err:
                print "ERROR: EXCEPTION DURING RECEIVEING AND READING DATAGRAM - %s" % err
            except KeyboardInterrupt:
                print "DEBUG: KEYBOARD INTERRUPT (CTRL+C)"
                self.__del__()
                return
            else:
                
                self.msg_handle(msg)

    def msg_handle(self, msg):
        packet = pb_wrapper.SSL_WrapperPacket()
        packet.ParseFromString(msg)
        
        world.sanitize_packet(packet)
        print "Packet rcvd"


    def __del__(self):
        print "DEBUG: CLOSING SOCKET"
        self.socket.close()
        print "DEBUG: SOCKET CLOSED"

if __name__ == "__main__":
    serv = Server()
