from sock import Server
from threading import Thread
from time import sleep
import env
from twisted.internet import reactor, protocol
import json
import memcache
import pickle
import sys
world = env.World()

def pServer():
	s = Server()
	s.start()        

    
if __name__ == '__main__':
	host = "127.0.0.1:11211"
	try:
		host = sys.argv[1]
	except:
		pass
	mc = memcache.Client([host], debug=0)
	t = Thread(target=pServer)
	t.daemon = True
	t.start()
	while True:
		#sleep(0.1)
		d = pickle.dumps(world.robots_blue)
		y = pickle.dumps(world.robots_yellow)
		b = pickle.dumps(world.balls)
		
		mc.set("robots_blue",d )
		mc.set("robots_yellow", y)   	
		mc.set("balls", b)
