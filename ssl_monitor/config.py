## FIELD SETTINGS ##

FIELD_ID = '' # Queryable ID of the field. e.g. 'field-a', or 'training-field' etc.
FIELD_LABEL = '' # Custom text label for Field. used for displaying purposes on web. . e.g. 'Field A'


## SSL VISION SETTINGS ##

SSL_VISION_MULTICAST_ADDRESS = '224.5.23.2' # Multicast Group Address. default is '224.5.23.2'
SSL_VISION_MULTICAST_PORT = 10020

